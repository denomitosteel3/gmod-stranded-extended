
Added:
  •Mysql support (https://github.com/FredyH/MySQLOO).
  
Modified:
   •Umsg replaced with net.
   •Character is saved in sv.db instead of being written to a file (or mysql database).
   •Combinations and unlocks settings moved to config folder

In plans:
   •Transition by locations when there are several servers.
   •Translation into different languages.
   •Support FPP,
Save

Workshop: https://steamcommunity.com/sharedfiles/filedetails/?id=3101752602
