local GM = GM or GAMEMODE
local meta = FindMetaTable("Player")

if CLIENT then 
	LANG = CreateClientConVar("lang", "english", true, true, "you lang")

	cvars.AddChangeCallback("lang", function(_, _, value_new)
	    local lang = LANGS[value_new]

		if !lang then 
			lang = LANGS["english"]
		end

		for k, v in pairs(lang) do 
			language.Add(k, v)
		end
	end)
end

LANGS = {}
local sctdlang = "english"

function AddLangPhrase(phrase, trs) 
	LANGS[sctdlang][phrase] = trs
end

function GetLangList() 
	local tbl = {}
	for lang, v in SortedPairs(LANGS) do 
		table.insert(tbl, lang)
	end

	return tbl
end

function meta:Tst(phrase, ...) 
	local lang = "english"
	
	if CLIENT then 
		lang = LANG:GetString()
	else 
		lang = self:GetInfo("lang")
	end
	local translate_phrase = phrase
	
	if LANGS[lang] then 
		if LANGS[lang][phrase] then
			translate_phrase = LANGS[lang][phrase]
		end
	elseif LANGS["english"][phrase] then
	 	translate_phrase = LANGS["english"][phrase] 
	end

	if #{...} > 0 then
		return string.format(translate_phrase, ...)
	end

	return translate_phrase
end

if CLIENT then 
	function TstPh(phrase, ...) 
		return LocalPlayer():Tst(phrase, ...)
	end
end

local function RegLang(lang) 
	LANGS[lang] = {}
end

local fol = GM.FolderName .. "/gamemode/language/"
local files, folders = file.Find(fol .. "*", "LUA")

for _, v in SortedPairs(files) do 
	if string.GetExtensionFromFilename(v) != "lua" then continue end 

	sctdlang = string.gsub(v, ".lua", "", 1) 
	RegLang(sctdlang) 

	AddCSLuaFile(GM.FolderName .. "/gamemode/language/"..v)
	include(GM.FolderName .. "/gamemode/language/"..v)
end

if CLIENT then
	timer.Create("loadlang", 10, 1, function() 
		local lang = LANGS[LANG:GetString()]

		if !lang then 
			lang = LANGS["english"]
		end

		for k, v in pairs(lang) do 
			language.Add(k, v)
		end
	end)
end