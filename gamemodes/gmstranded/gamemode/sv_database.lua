hook.Add("DBLoad", "MsgOnDBLoad", function(isError) 
    MsgN("Database load!")
end)


if database.cfg.type == 1 then
    function database.RawQuery(qs, callback, errorCallback) 
        if !qs then return end 
        
        local data = sql.Query(qs)

        if callback then
            callback(data)
        end
    end
    //example
    //database.SimpleUniqueQuery("player", nil, "id", self:SteamID(), "value", util.TableToJSON(tbl)) 
    function database.SimpleUniqueQuery(dbname, callback, ...) 
        local tbl = {...}
        local insert_into = {}
        local values = {}
       
        for k, v in ipairs(tbl) do 
            if k % 2 != 0 then
                table.insert(insert_into, v)
            else 
                table.insert(values, isstring(v) and "'"..(v).."'" or tostring(v))
            end
        end

        local insert_into_str = string.Implode(", ", insert_into)
        insert_into_str = "INSERT INTO ".. dbname .. " " .. "(".. insert_into_str .. ")"
        local values_str = string.Implode(", ", values)
        values_str = "VALUES ".. "(".. values_str .. ")"


        local dublicate = {}

        for k, v in ipairs(insert_into) do 
            if k == 1 then continue end 

            table.insert(dublicate, v .. " = ".. "excluded."..v)
        end

        local dublicate_str = string.Implode(", ", dublicate)     
        dublicate_str = string.format("ON CONFLICT(%s) DO UPDATE SET " .. dublicate_str, insert_into[1])

        local qs = insert_into_str.. " " ..values_str .. " " .. dublicate_str
        database.RawQuery(qs, callback)
    end

    hook.Add("Initialize", "DB", function() 
        hook.Run("DBLoad")
    end)
elseif database.cfg.type == 2 then
    require("mysqloo")

    local databaseObject = mysqloo.connect(database.cfg.host, database.cfg.username, database.cfg.password, database.cfg.name, database.cfg.port)
    databaseObject.onConnected = function() 
        hook.Run("DBLoad")
    end
    databaseObject.onConnectionFailed = function() hook.Run("DBErrorLoad")end
    databaseObject:connect()

    function database.RawQuery(qs, callback, errorCallback) 
        if !qs then return end 
        
        
        local query1 = databaseObject:query(qs)

        function query1:onSuccess(data)
            if !callback then return end

            callback(data or {})
        end

        query1.onError = function(self, err, sql)
            if !errorCallback then return end 
            
            errorCallback(self, sql)    
        end

        query1:start()
    end

    function database.SimpleUniqueQuery(dbname, callback, ...) 
        local tbl = {...}
        local insert_into = {}
        local values = {}
       
        for k, v in ipairs(tbl) do 
            if k % 2 != 0 then
                table.insert(insert_into, v)
            else 
                table.insert(values, isstring(v) and "'"..(v).."'" or v)
            end
        end

        local insert_into_str = string.Implode(", ", insert_into)
        insert_into_str = "INSERT INTO ".. dbname .. " " .. "(".. insert_into_str .. ")"
        local values_str = string.Implode(", ", values)
        values_str = "VALUES ".. "(".. values_str .. ")"


        local dublicate = {}

        for k, v in ipairs(insert_into) do 
            if k == 1 then continue end 

            table.insert(dublicate, v .. " = VALUES"..  "(" .. (v).. ")")
        end

        local dublicate_str = string.Implode(", ", dublicate)     
        dublicate_str = "ON DUPLICATE KEY UPDATE " .. dublicate_str

        local qs = insert_into_str.. " " ..values_str .. " " .. dublicate_str
        database.RawQuery(qs, callback)
    end

end

