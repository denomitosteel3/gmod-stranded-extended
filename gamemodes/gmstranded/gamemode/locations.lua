net.Receive("locations.RequestTrans", function(len, ply) 
	local loc = ply:GetNWString("LocationTrans", nil)

	if !(!ply.NextLocationRequest or CurTime() > ply.NextLocationRequest) then
		return
	end

	if locations.list[loc] then		
		database.RawQuery(string.format([[SELECT * FROM `gms_location` WHERE id = '%s']], loc ), function(data) 
			if !IsValid(ply) then return end

			local row = data[1]

			if data and #data > 0 then
				if os.time() - row.uptime <= 50 then
					ply.Location = loc
					ply:SaveCharacter()
					ply:SendLua(string.format([[permissions.AskToConnect("%s")]], locations.list[loc].adress))
				end
			end
		end)
	end

	ply.NextLocationRequest = CurTime() + 3
end)

hook.Add("PostGamemodeLoaded", "ClearDatabaseLocationUptime", function() 
	database.RawQuery("TRUNCATE TABLE gms_location;", function() 
		database.SimpleUniqueQuery("gms_location", nil, "id", GAMEMODE.GetLocation(), "uptime", os.time()) 
	end)
	
	timer.Create("locations_uptime", 15, 0, function() 
		database.SimpleUniqueQuery("gms_location", nil, "id", GAMEMODE.GetLocation(), "uptime", os.time()) 
	end)
end)

local next_connect = {}
hook.Add("CheckPassword", "KickNotThatLocation", function(id) 
	id = util.SteamIDFrom64(id)

	if next_connect[id] then
		return false, "Connectivity is available through: " .. math.Round(timer.TimeLeft("remove_steamid_"..id), 1)
	end

	if GAMEMODE.GetLocation() then 
		database.RawQuery(string.format([[SELECT * FROM `gms_player` WHERE id = '%s']], id ), function(data) 
			if data and #data > 0 then
				local tbl = util.JSONToTable(data[1].value)

				if !( tbl.Location == GAMEMODE.GetLocation()) then
					game.KickID(id, "You are in a different location!\nYour location: " .. tbl.Location)
				end
			else 
				if !(GAMEMODE.GetLocation() == locations.DefaultLocation) then
					game.KickID(id, "You have to go to the default location!")
				end
			end
		end)
	end

	next_connect[id] = true
	timer.Create("remove_steamid_"..id, 5, 0, function() 
		next_connect[id] = nil
	end)
end)