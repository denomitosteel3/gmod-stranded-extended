ENT.Type = "anim"
ENT.PrintName = "Location transition"
ENT.Author = "Denomito"
ENT.Spawnable = false
ENT.Model = "models/hunter/blocks/cube025x025x025.mdl"

function ENT:SetupDataTables() 
	self:NetworkVar("Vector", 0, "FindMaxs")
	self:NetworkVar("Vector", 1, "FindMins")
	self:NetworkVar("String", 0, "Location")
end

