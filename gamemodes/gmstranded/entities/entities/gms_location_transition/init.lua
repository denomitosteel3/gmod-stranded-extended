AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include( "shared.lua" )

function ENT:Initialize()
    if self.Model then
        self:SetModel(self.Model)
    end
    
    if self.Skin then 
        self:SetSkin(self.Skin)
    end

    if self.CustomMaterial then 
        self:SetMaterial(self.CustomMaterial)
    end

    self:SetMoveType(MOVETYPE_FLY)
    self:SetSolid(SOLID_VPHYSICS)
    self:SetUseType(SIMPLE_USE)
    self:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
    self:DrawShadow(false)
    self:AddEFlags(EFL_FORCE_CHECK_TRANSMIT)
end

local function getPlayerInBox(maxs, mins) 
    local tbl = {}

    for _, v in ipairs(ents.FindInBox(maxs, mins)) do 
        if !(IsValid(v) and v:IsPlayer() and v:Alive()) then continue end

        tbl[v:UserID()] = v
    end

    return tbl
end

hook.Add("PlayerPostThink", "CheckInTrans", function(ply) 
    if !ply.location_check or CurTime() > ply.location_check then
        local find = nil
        for key, ent in ipairs(ents.FindByClass("gms_location_transition")) do 
            local plys = getPlayerInBox(ent:GetFindMaxs(), ent:GetFindMins()) 

            if plys[ply:UserID()] then
                if ent:GetLocation() != ply:GetNWString("LocationTrans", nil) then
                    ply:SetNWString("LocationTrans", ent:GetLocation())
                    net.Start("locations.RequestTrans")
                        net.WriteString(ent:GetLocation())
                    net.Send(ply)
                end

                find = true
                break
            end
        end

        if !find then
            ply:SetNWString("LocationTrans", nil)
        end

        ply.location_check = CurTime() + 1
    end
end)