include( "shared.lua" )

local function DrawCube(maxs, mins)
    local vertices = {
        Vector(maxs.x, maxs.y, maxs.z),
        Vector(maxs.x, maxs.y, mins.z),
        Vector(maxs.x, mins.y, maxs.z),
        Vector(maxs.x, mins.y, mins.z),
        Vector(mins.x, maxs.y, maxs.z),
        Vector(mins.x, maxs.y, mins.z),
        Vector(mins.x, mins.y, maxs.z),
        Vector(mins.x, mins.y, mins.z)
    }

    -- Draw the edges of the cube
    render.DrawLine(vertices[1], vertices[2], color_white)
    render.DrawLine(vertices[1], vertices[3], color_white)
    render.DrawLine(vertices[1], vertices[5], color_white)
    render.DrawLine(vertices[2], vertices[4], color_white)
    render.DrawLine(vertices[2], vertices[6], color_white)
    render.DrawLine(vertices[3], vertices[4], color_white)
    render.DrawLine(vertices[3], vertices[7], color_white)
    render.DrawLine(vertices[4], vertices[8], color_white)
    render.DrawLine(vertices[5], vertices[6], color_white)
    render.DrawLine(vertices[5], vertices[7], color_white)
    render.DrawLine(vertices[6], vertices[8], color_white)
    render.DrawLine(vertices[7], vertices[8], color_white)
end


function ENT:Draw()
	local dev = GetConVar("developer")

	if dev:GetInt() >= 2 and LocalPlayer():IsSuperAdmin() then 
		self:DrawModel()

		DrawCube(self:GetFindMaxs(), self:GetFindMins())
	end	
end