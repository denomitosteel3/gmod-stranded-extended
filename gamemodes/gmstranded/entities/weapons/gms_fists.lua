
AddCSLuaFile()

SWEP.Slot = 1
SWEP.SlotPos = 1

SWEP.Base = "gms_base_weapon"
SWEP.PrintName = "Fists"
SWEP.ViewModel = Model( "models/weapons/c_arms.mdl" )
SWEP.ViewModelFOV = 54
SWEP.WorldModel = ""
SWEP.UseHands = true

SWEP.Purpose = "Pick up stuff, as well as poor harvesting."
SWEP.Instructions = "Primary fire: Attack/Harvest"
SWEP.HitDistance = 54

function SWEP:Deploy()

	local speed = GetConVarNumber( "sv_defaultdeployspeed" )

	local vm = self:GetOwner():GetViewModel()
	if IsValid(vm) then
		vm:SendViewModelMatchingSequence( vm:LookupSequence( "fists_draw" ) )
		vm:SetPlaybackRate( speed )

		self:SetNextPrimaryFire( CurTime() + vm:SequenceDuration() / speed )
		self:SetNextSecondaryFire( CurTime() + vm:SequenceDuration() / speed )
	end

	return true

end

function SWEP:PrimaryAttack2() 
	local owner = self:GetOwner()

	owner:SetAnimation( PLAYER_ATTACK1 )

	local anim = "fists_left"
	if ( right ) then anim = "fists_right" end

	local vm = owner:GetViewModel()
	vm:SendViewModelMatchingSequence( vm:LookupSequence( anim ) )
end